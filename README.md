# My Sway Window Manager Configuration

What's Sway? Its a tiling window manager meant for the Wayland display server. (Google the terms if they are foreign)

While this is meant as a way to port my config to multiple computers, feel free to use or modify it as you wish!
